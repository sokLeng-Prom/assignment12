<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    use HasFactory;
    protected $fillable=[
        "title",
        "user_id",
        "status",
    ];
    public function user(){
        return $this -> belongTo(User::class);
    }

}
