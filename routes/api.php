<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TodoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/todo', [TodoController::class, 'index']);
Route::post('/signup',[LoginController::class, 'store']); 
Route::post('/login',[LoginController::class, 'login']); 
Route::group(['prefix' => 'todo', 'middleware' => 'auth:api' ], function()
{
    Route::put('/{task_id}', [TodoController::class, 'edit']);
    Route::post('/', [TodoController::class, 'store']); 
    Route::get('/{user_id}', [TodoController::class, 'getUserTodo']);
    Route::delete('/{task_id}', [TodoController::class, 'delete']); 
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
